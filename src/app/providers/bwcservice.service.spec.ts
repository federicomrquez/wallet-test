import { TestBed } from '@angular/core/testing';

import { BWCServiceService } from './bwcservice.service';

describe('BWCServiceService', () => {
  let service: BWCServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BWCServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
