import { Component } from '@angular/core';
import { Utils } from 'bitcore-wallet-client/ts_build/lib/common';
import { Key } from 'bitcore-wallet-client/ts_build/lib/key';
import { BWCServiceService } from '../providers/bwcservice.service';
import { BitcoreLib } from 'crypto-wallet-core';
import * as _ from 'lodash';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page {
  client;
  key;

  constructor(
    private bwcSeriviceService : BWCServiceService
  ) {}
  clientText: string;

  ionViewWillEnter() {}

  createWallet() {
    const client = this.bwcSeriviceService.getClient();
    const key = new Key({
      seedType: 'new',
      useLegacyCoinType: false,
      useLegacyPurpose: false,
    });

    client.fromString(
      key.createCredentials('test', {
        coin: 'btc',
        network: 'testnet',
        account: 0,
        n: 1,
        m: 1,
      })
    );

    client.credentials.m = 1;
    console.log(client.credentials);

    client.createWallet(
      'Test Wallet',
      'Federico',
      client.credentials.m,
      client.credentials.n,
      {
        coin: 'btc',
        network: 'testnet',
        singleAddress: false,
        useNativeSegwit: true,
        walletPrivKey: client.credentials.walletPrivKey,
      },
      (err) => {
        if (err) {
          console.log('error: ', err);
          return;
        } else {
          console.log('Éxito');
        }
      }
    );
  }

  _createWallet() {
    // this.client.credentials = this.key.createCredentials("test", { coin: 'btc', network: 'livenet', account: 0, n: 2, m: 2 });
    // console.log(this.client.credentials);
    
    // this.client.createWallet("My Wallet", "Irene", 2, 2, {network: 'livenet'}, function(err, secret) {
    //   if (err) {
    //     console.log('error: ',err); 
    //     return;
    //   };
    //   // Handle err
    //   console.log('Wallet Created. Share this secret with your copayers: ' + secret);
    //   this.clientText = this.client.export();
    // });

    const message = ["Hola buenas tardes", "Something else", "POST"];
    const privKey = '3d7384c5695070d5a07bab99847793e73464dafba1d7bc3aca5c1391e8789a98';

    // const signature = Utils.signMessage(message, privKey);
    // console.log(signature);

    var priv = new BitcoreLib.PrivateKey(privKey);
    // console.log(priv);
    const flattenedMessage = _.isArray(message) ? _.join(message) : message;
    // console.log(flattenedMessage);
    var hash = Utils.hashMessage(flattenedMessage);
    // console.log(hash);
    // const signature = BitcoreLib.crypto.ECDSA.sign(hash, priv, 'little').toString();

    // var ecdsa = BitcoreLib.crypto.ECDSA();

    // ecdsa.set({
    //   hashbuf: hash,
    //   endian: 'little',
    //   privkey: priv
    // });

    // console.log(ecdsa);

    // ecdsa.sign();

    var sig = BitcoreLib.crypto.ECDSA().set({
        hashbuf: hash,
        endian: 'little',
        privkey: priv
      }).sign().sig;
    
    // console.log(sig);
    // var buf = sig.toDER();
    // console.log(sig.r);
    // console.log(sig.s);


    // var opts = { endian: 'little' };
    // var buf, hex;
  
    // hex = sig.r.toString(16, 2);
    // buf = Buffer.from(hex, 'hex');

    // if (typeof opts !== 'undefined' && opts.endian === 'little') {
    //   var buf2 = Buffer.alloc(buf.length);
    //   for (var i = 0; i < buf.length; i++) {
    //     buf2[i] = buf[buf.length - 1 - i];
    //   }
    //   buf = buf2;
    // }

    // var rnbuf = buf;

    // console.log(rnbuf);

    // buf = null;
    // hex = null;
  
    // hex = sig.s.toString(16, 2);
    // buf = Buffer.from(hex, 'hex');

    // if (typeof opts !== 'undefined' && opts.endian === 'little') {
    //   var buf2 = Buffer.alloc(buf.length);
    //   for (var i = 0; i < buf.length; i++) {
    //     buf2[i] = buf[buf.length - 1 - i];
    //   }
    //   buf = buf2;
    // }

    // var snbuf = buf;
    // console.log(snbuf);

    // buf = null;
    // var BN = BitcoreLib.crypto.BN;

    // var rnbuf = new BN(sig.s);
    // var snbuf = sig.s.toBuffer();

    var rnbuf = sig.r.toBuffer();
    var snbuf = sig.s.toBuffer();
  
    var rneg = rnbuf[0] & 0x80 ? true : false;
    var sneg = snbuf[0] & 0x80 ? true : false;
  
    var rbuf = rneg ? Buffer.concat([Buffer.from([0x00]), rnbuf]) : rnbuf;
    var sbuf = sneg ? Buffer.concat([Buffer.from([0x00]), snbuf]) : snbuf;
  
    var rlength = rbuf.length;
    var slength = sbuf.length;
    var length = 2 + rlength + 2 + slength;
    var rheader = 0x02;
    var sheader = 0x02;
    var header = 0x30;
  
    var der = Buffer.concat([Buffer.from([header, length, rheader, rlength]), rbuf, Buffer.from([sheader, slength]), sbuf]);

    var buf = der;

    console.log(buf);
    var str = buf.toString('hex');
    console.log(str);
    // var $ = BitcoreLib.util.preconditions;

    // var hashbuf = hash;
    // var privkey = priv;
    // var d = privkey.bn;

    // console.log(hashbuf);
    // console.log(privkey);
    // console.log(d);

    // $.checkState(hashbuf && privkey && d, new Error('invalid parameters'));
    // $.checkState(BitcoreLib.util.buffer.isBuffer(hashbuf) && hashbuf.length === 32, new Error('hashbuf must be a 32 byte buffer'));

    // let endian;

    // var e = BitcoreLib.crypto.BN.fromBuffer(hashbuf, endian ? {
    //   endian: endian
    // } : undefined);

    // var obj = BitcoreLib.crypto.ECDSA._findSignature(d, e);
    // // obj.compressed = this.pubkey.compressed;

    // const sig = new BitcoreLib.crypto.Signature(obj);
    
    // console.log(sig);
  }
}